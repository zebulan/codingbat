def common_end(a, b):
    return True if a[0] == b[0] or a[-1] == b[-1] else False

assert common_end([1, 2, 3], [7, 3]) is True
assert common_end([1, 2, 3], [7, 3, 2]) is False
assert common_end([1, 2, 3], [1, 3]) is True
