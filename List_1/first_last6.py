def first_last6(nums):
    return True if nums[0] == 6 or nums[-1] == 6 else False

assert first_last6([1, 2, 6]) is True
assert first_last6([6, 1, 2, 3]) is True
assert first_last6([13, 6, 1, 2, 3]) is False
