def has23(nums):
    return 2 in nums or 3 in nums

assert has23([2, 5]) is True
assert has23([4, 3]) is True
assert has23([4, 5]) is False
