def reverse3(nums):
    return nums[::-1]

assert reverse3([1, 2, 3]) == [3, 2, 1]
assert reverse3([5, 11, 9]) == [9, 11, 5]
assert reverse3([7, 0, 0]) == [0, 0, 7]
