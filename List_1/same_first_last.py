def same_first_last(nums):
    return True if len(nums) >= 1 and nums[0] == nums[-1] else False

assert same_first_last([1, 2, 3]) is False
assert same_first_last([1, 2, 3, 1]) is True
assert same_first_last([1, 2, 1]) is True
