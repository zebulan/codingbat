def sum2(nums):
    return sum(nums) if len(nums) < 2 else sum(nums[:2])

assert sum2([1, 2, 3]) == 3
assert sum2([1, 1]) == 2
assert sum2([1, 1, 1, 1]) == 2
