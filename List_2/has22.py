def has22(nums):
    txt = ''.join(str(a) for a in nums)
    return True if txt.count('22') > 0 else False

assert has22([1, 2, 2]) is True
assert has22([1, 2, 1, 2]) is False
assert has22([2, 1, 2]) is False
