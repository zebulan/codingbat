def sum13(nums):
    total = 0
    skip_next = False
    for a in nums:
        if skip_next:
           skip_next = False
        else:
            if a == 13:
                skip_next = True
            else:
                total += a
                skip_next = False
    return total

assert sum13([1, 2, 2, 1]) == 6
assert sum13([1, 1]) == 2
assert sum13([1, 2, 2, 1, 13]) == 6
