def sum67(nums):
    if 6 not in nums:
        return sum(nums)
    total = 0
    skip = False
    for a in nums:
        if a == 6:
            skip = True
        elif a == 7 and skip:
            skip = False
            continue
        if not skip:
            total += a
    return total

assert sum67([1, 2, 2]) == 5
assert sum67([1, 2, 2, 6, 99, 99, 7]) == 5
assert sum67([1, 1, 6, 7, 2]) == 4
