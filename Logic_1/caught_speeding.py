def caught_speeding(speed, is_birthday):
    if 61 <= speed <= 80 and not is_birthday or \
       65 < speed <= 85 and is_birthday:
        return 1
    elif speed <= 60 or\
       speed <= 65 and is_birthday:
        return 0
    return 2

assert caught_speeding(60, False) == 0
assert caught_speeding(65, False) == 1
assert caught_speeding(65, True) == 0
