def cigar_party(cigars, is_weekend):
    return 40 <= cigars <= 60 or cigars >= 40 and is_weekend

assert cigar_party(30, False) is False
assert cigar_party(50, False) is True
assert cigar_party(70, True) is True
