def in1to10(n, outside_mode):
    if outside_mode and not(1 < n < 10) or\
       not outside_mode and 1 <= n <= 10:
        return True
    return False

assert in1to10(5, False) is True
assert in1to10(11, False) is False
assert in1to10(11, True) is True
