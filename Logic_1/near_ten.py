def near_ten(num):
    mod = divmod(num, 10)[1]
    return 0 <= mod <= 2 or 8 <= mod <= 10

assert near_ten(12) is True
assert near_ten(17) is False
assert near_ten(19) is True
