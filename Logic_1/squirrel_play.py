def squirrel_play(temp, is_summer):
    limit = 90
    if is_summer:
        limit += 10
    return 60 <= temp <= limit

assert squirrel_play(70, False) is True
assert squirrel_play(95, False) is False
assert squirrel_play(95, True) is True
