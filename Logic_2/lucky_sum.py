def lucky_sum(a, b, c):
    total = 0
    for each in [c, b, a]:
        if each == 13:
            total = 0
        else:
            total += each
    return total

assert lucky_sum(1, 2, 3) == 6
assert lucky_sum(1, 2, 13) == 3
assert lucky_sum(1, 13, 3) == 1
