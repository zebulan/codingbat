def make_bricks(small, big, goal):
    return not(goal > big * 5 + small or goal % 5 > small)

assert make_bricks(3, 1, 8) is True
assert make_bricks(3, 1, 9) is False
assert make_bricks(3, 2, 10) is True
