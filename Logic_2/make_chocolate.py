def make_chocolate(small, big, goal):
    big *= 5
    if small < goal % 5 or big + small < goal:
        return -1
    small = goal - big
    return small % 5 if small < 0 else small

assert make_chocolate(4, 1, 9) == 4
assert make_chocolate(4, 1, 10) == -1
assert make_chocolate(4, 1, 7) == 2
