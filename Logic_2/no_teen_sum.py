def fix_teen(n):
    return 0 if 13 <= n < 15 or 17 <= n <= 19 else n


def no_teen_sum(a, b, c):
    return fix_teen(a) + fix_teen(b) + fix_teen(c)

assert no_teen_sum(1, 2, 3) == 6
assert no_teen_sum(2, 13, 1) == 3
assert no_teen_sum(2, 1, 14) == 3
