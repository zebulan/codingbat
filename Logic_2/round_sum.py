def round10(num):
    rem = num % 10
    if rem == 5:
        return num + 5
    elif rem > 5:
        return num + 10 % rem
    return num - rem


def round_sum(a, b, c):
    return round10(a) + round10(b) + round10(c)

assert round_sum(16, 17, 18) == 60
assert round_sum(12, 13, 14) == 30
assert round_sum(6, 4, 4) == 10
