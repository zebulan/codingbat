def combo_string(a, b):
    return ''.join((a, b, a)) if len(a) < len(b) else ''.join((b, a, b))

assert combo_string('Hello', 'hi') == 'hiHellohi'
assert combo_string('hi', 'Hello') == 'hiHellohi'
assert combo_string('aaa', 'b') == 'baaab'
