def first_two(txt):
    return txt if len(txt) < 2 else txt[:2]

assert first_two('Hello') == 'He'
assert first_two('abcdefg') == 'ab'
assert first_two('ab') == 'ab'
