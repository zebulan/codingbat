def hello_name(name):
    return 'Hello {0}!'.format(name)

assert hello_name('Bob') == 'Hello Bob!'
assert hello_name('Alice') == 'Hello Alice!'
assert hello_name('X') == 'Hello X!'
assert hello_name('') == 'Hello !'
