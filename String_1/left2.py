def left2(txt):
    return ''.join((txt[2:], txt[:2]))

assert left2('Hello') == 'lloHe'
assert left2('java') == 'vaja'
assert left2('Hi') == 'Hi'
