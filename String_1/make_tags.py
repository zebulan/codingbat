def make_tags(tag, word):
    return '<{0}>{1}</{0}>'.format(tag, word)

assert make_tags('i', 'Yay') == '<i>Yay</i>'
assert make_tags('i', 'Hello') == '<i>Hello</i>'
assert make_tags('cite', 'Yay') == '<cite>Yay</cite>'
