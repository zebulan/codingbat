def cat_dog(txt):
    return txt.count('cat') == txt.count('dog')

assert cat_dog('catdog') is True
assert cat_dog('catcat') is False
assert cat_dog('1cat1cadodog') is True
