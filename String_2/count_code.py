def count_code(txt):
    cnt = 0
    for letter in set(list(txt)):
        chk = ''.join(('co', letter, 'e'))
        current = txt.count(chk)
        if current > 0:
            cnt += current
    return cnt

assert count_code('aaacodebbb') == 1
assert count_code('codexxcode') == 2
assert count_code('cozexxcope') == 2
