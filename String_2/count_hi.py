def count_hi(txt):
    return txt.count('hi')

assert count_hi('abc hi ho') == 1
assert count_hi('ABChi hi') == 2
assert count_hi('hihi') == 2
