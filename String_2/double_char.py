def double_char(txt):
    return ''.join(str(a * 2) for a in txt)

assert double_char('The') == 'TThhee'
assert double_char('AAbb') == 'AAAAbbbb'
assert double_char('Hi-There') == 'HHii--TThheerree'
