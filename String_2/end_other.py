def end_other(a, b):
    a, b = a.lower(), b.lower()
    return a.endswith(b) or b.endswith(a)

assert end_other('Hiabc', 'abc') is True
assert end_other('AbC', 'HiaBc') is True
assert end_other('abc', 'abXabc') is True
