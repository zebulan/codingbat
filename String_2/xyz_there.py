def xyz_there(txt):
    return txt.count('xyz') > txt.count('.xyz')

assert xyz_there('abcxyz') is True
assert xyz_there('abc.xyz') is False
assert xyz_there('xyz.abc') is True
