def diff21(n):
    diff = abs(21 - n)
    return diff * 2 if n > 21 else diff

assert diff21(19) == 2
assert diff21(10) == 11
assert diff21(21) == 0
