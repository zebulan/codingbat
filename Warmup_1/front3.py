def front3(txt):
    return txt * 3 if len(txt) < 3 else txt[:3] * 3

assert front3('Java') == 'JavJavJav'
assert front3('Chocolate') == 'ChoChoCho'
assert front3('abc') == 'abcabcabc'
