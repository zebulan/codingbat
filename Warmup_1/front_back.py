def front_back(txt):
    return ''.join((txt[-1], txt[1:-1], txt[0])) if len(txt) > 1 else txt

assert front_back('code') == 'eodc'
assert front_back('a') == 'a'
assert front_back('ab') == 'ba'
