def missing_char(txt, n):
    return ''.join(a for i, a in enumerate(txt) if not i == n)

assert missing_char('kitten', 1) == 'ktten'
assert missing_char('kitten', 0) == 'itten'
assert missing_char('kitten', 4) == 'kittn'
