def near_hundred(n):
    return abs(n - 100) <= 10 or abs(n - 200) <= 10

assert near_hundred(93) is True
assert near_hundred(90) is True
assert near_hundred(89) is False
