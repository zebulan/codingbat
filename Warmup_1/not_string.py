def not_string(txt):
    return ' '.join(('not', txt)) if not txt.startswith('not') else txt

assert not_string('candy') == 'not candy'
assert not_string('x') == 'not x'
assert not_string('not bad') == 'not bad'
