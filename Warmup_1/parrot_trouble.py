def parrot_trouble(talking, hour):
    return talking and (hour < 7 or hour > 20)

assert parrot_trouble(True, 6) is True
assert parrot_trouble(True, 7) is False
assert parrot_trouble(False, 6) is False
