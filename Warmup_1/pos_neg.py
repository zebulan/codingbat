def pos_neg(a, b, negative):
    if negative:
        return a < 0 and b < 0
    else:
        return a < 0 < b or a > 0 > b

assert pos_neg(1, -1, False) is True
assert pos_neg(-1, 1, False) is True
assert pos_neg(-4, -5, True) is True
