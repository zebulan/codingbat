def sleep_in(weekday, vacation):
    return True if not weekday or vacation else False

assert sleep_in(False, False) is True
assert sleep_in(True, False) is False
assert sleep_in(False, True) is True
