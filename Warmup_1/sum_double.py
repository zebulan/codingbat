def sum_double(a, b):
    return a + b if not a == b else (a + b) * 2

assert sum_double(1, 2) == 3
assert sum_double(3, 2) == 5
assert sum_double(2, 2) == 8
