def array123(nums):
    return ''.join(str(a) for a in nums).find('123') > -1

assert array123([1, 1, 2, 3, 1]) is True
assert array123([1, 1, 2, 4, 1]) is False
assert array123([1, 1, 2, 1, 2, 3]) is True
