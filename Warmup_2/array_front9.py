def array_front9(nums):
    for i, num in enumerate(nums):
        if i < 4 and num == 9:
            return True
        elif i >= 4:
            break
    return False

assert array_front9([1, 2, 9, 3, 4]) is True
assert array_front9([1, 2, 3, 4, 9]) is False
assert array_front9([1, 2, 3, 4, 5]) is False
