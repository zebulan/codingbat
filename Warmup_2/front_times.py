def front_times(txt, n):
    return txt * n if len(txt) < 3 else txt[:3] * n

assert front_times('Chocolate', 2) == 'ChoCho'
assert front_times('Chocolate', 3) == 'ChoChoCho'
assert front_times('Abc', 3) == 'AbcAbcAbc'
