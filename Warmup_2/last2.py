def last2(txt):
    if len(txt) <= 2:
        return 0
    end = txt[-2:]
    return sum(1 for a in range(len(txt) - 2) if txt[a:a+2] == end)

assert last2('hixxhi') == 1
assert last2('xaxxaxaxx') == 1
assert last2('axxxaaxx') == 2
