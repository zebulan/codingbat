def two_chunks(txt):
    return [txt[a:a+2] for a in range(len(txt) - 1)]

def string_match(a, b):
    return sum(1 for a, b in zip(two_chunks(a), two_chunks(b)) if a == b)

assert string_match('xxcaazz', 'xxbaaz') == 3
assert string_match('abc', 'abc') == 2
assert string_match('abc', 'axc') == 0
