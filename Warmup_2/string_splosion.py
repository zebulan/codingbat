def string_splosion(txt):
    return ''.join(txt[:a] for a in range(len(txt)+1))

assert string_splosion('Code') == 'CCoCodCode'
assert string_splosion('abc') == 'aababc'
assert string_splosion('ab') == 'aab'
